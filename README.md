# TG Channel Cloner Workers

NOTE: Bot must be added to Both Channels, Write Permission into Destination Channel

## Workers.js File is used to Clone Old Messages Only

* First Make KV Namespace
* then add values as mentioned in photo

![https://i.imgur.com/7p1lgrn.png](https://i.imgur.com/7p1lgrn.png)

* then bind it to your Worker

![https://i.imgur.com/ZxoR9FP.png](https://i.imgur.com/ZxoR9FP.png)

* then start cron job, set minutes to 1 or 2 or 5 as per your choice. recommended is minimum 2 minutes.

![https://i.imgur.com/XJyMszm.png](https://i.imgur.com/XJyMszm.png)

* then paste the script and add values
* then open the cloudflare workers URL once to setup webhook
* then go to bot and use commands as given below

#### Commands List

* /startbot
* /stopbot
* /status
* /from_channel <CHANNELIDHERE>
* /to_channel <CHANNELIDHERE>
* /from_message <MessageID>
* /to_message <MessageID>

## auto_forward_new_messages.js is for New Messages Auto Forwarding Service

* Paste Script and add values
* then open the cloudflare workers URL once to setup webhook
* If you added 4 FROM Channels, then it'll handle all 4 Channels
* It will forward all messages from FROM Channels to All Channels added in to_channel option.
* for Each Clone, u need different Bot and Different Worker.

## Errors

````
{"ok":false,"error_code":400,"description":"Bad Request: chat not found"}
````

* Above Error Means you have not added the channel in destination (to_channel). If you have already added, then remove and add again, sometimes this needs to be done.

## Disclaimer

* I just wrote the code, didn't had time to clean, but it works.
* I'm not responsible for any abuse, handle with care and don't abuse.
* The Code is avaiable with License.
